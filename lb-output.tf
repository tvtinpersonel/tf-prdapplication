output "lb_internal_kaprdapp_ip" {
  value = module.lb_internal_kaprdapp.*.azurerm_lb_ip
}

output "lb_internal_kaprdapp_id" {
  value = module.lb_internal_kaprdapp.azurerm_lb_id
}

output "lb_internal_kaprdapp_frontend_conf" {
  value = module.lb_internal_kaprdapp.azurerm_lb_frontend_ip_configuration
}

output "lb_internal_kaprdapp_probe_ids" {
  value = module.lb_internal_kaprdapp.azurerm_lb_probe_ids
}

output "lb_internal_kaprdapp_backend_pool_id" {
  value = module.lb_internal_kaprdapp.azurerm_lb_backend_address_pool_id[0]
}

output "lb_internal_kaprdapp_backend_pool_02_id" {
  value = module.lb_internal_kaprdapp.azurerm_lb_backend_address_pool_id[1]
}