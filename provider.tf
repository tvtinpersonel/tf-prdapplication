
variable "subscription_id" {
    default = "2edcaa0b-ed18-4de0-977c-bf00e500f8e7"
}
#variable "client_id" {
#    default = ""
#}
#variable "client_secret" {
#    default = ""
#}
variable "tenant_id" {
    default = "76e454cc-a54c-477d-97b4-725f43439ab6"
}


provider "azurerm" {
    version = "=1.44.0"
    subscription_id = var.subscription_id
    #client_id       = "${var.client_id}"
    #client_secret   = "${var.client_secret}"
    tenant_id       = var.tenant_id
}