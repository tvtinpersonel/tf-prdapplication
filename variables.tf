variable "env" {
  default = "prod"
}

variable "project" {
  default = "kacalpweb"
}

variable "service" {
  default = "general"
}

variable "location" {
  default = "Southeast Asia"
}

variable "resource_group_name" {
  default = "test-tvtran-fpt"
}
variable "subnet_KAPrdWebSubnet" {
  default = "/subscriptions/2edcaa0b-ed18-4de0-977c-bf00e500f8e7/resourceGroups/test-tvtran-fpt/providers/Microsoft.Network/virtualNetworks/vn-dev-fpt-general/subnets/subnet-dev-fpt-general-external"
}

variable "subnet_KAPrdAppSubnet" {
  default = "/subscriptions/2edcaa0b-ed18-4de0-977c-bf00e500f8e7/resourceGroups/test-tvtran-fpt/providers/Microsoft.Network/virtualNetworks/vn-dev-fpt-general/subnets/subnet-dev-fpt-general-internal"
}


variable "sg_KAPrdAppSubnet" {
  default = "/subscriptions/2edcaa0b-ed18-4de0-977c-bf00e500f8e7/resourceGroups/test-tvtran-fpt/providers/Microsoft.Network/networkSecurityGroups/sg-dev-fpt-general-internal"
}

variable "sg_KAPrdWebSubnet" {
  default = "/subscriptions/2edcaa0b-ed18-4de0-977c-bf00e500f8e7/resourceGroups/test-tvtran-fpt/providers/Microsoft.Network/networkSecurityGroups/sg-dev-fpt-general-external"
}

variable "os_admin_user" {
  default = "tvtran"
}

variable "ssh_key_data" {
  default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDfYhaId03HajWYkogxNqUUcRctU/jmfpn6sdqIiLARPsZgAbgQQFxl/jrkjufhEMl+qwSSeekGwZxCscfqn/artV5B8B9uF5H+jyUpvw/drWJZQpk2hsEY6kVH+qJtxH2FQ1dwOhVxP87bK8dmoaupf6PMb3dZ7+I9d0+fG0BpOoqBdKH7xc+3LZ5KZfPgc3u1dnPFU6/dWY08Inln1BiW8UhJBiX+yKXZjy+al2vjIgeXoLY03IgWTcGbF8WEl7VIDYPM6oDv+QubV3uoYaMpnEXI+g6kRqPtSdc2XUAvDq4Z2LhSbg2vJDZX8jsRwgG3C3nvWMwlKWSVCDkhwqOP tin@cc-4a9b6aaa-7979b569d5-2s2t"
}

