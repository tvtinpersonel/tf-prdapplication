
# Crating private rules for bastion, this don't have dependencies to subnet sg. Must be defined full rules.
#resource "azurerm_network_security_group" "sg_bastion" {
#  name                = "sg-${var.env}-${var.project}-${var.service}-bastion"
#  location            = var.location
#  resource_group_name = var.resource_group_name

#  security_rule {
#    name                       = "AllowBastionPort"
#    priority                   = 300
#    direction                  = "Inbound"
#    access                     = "Allow"
#    protocol                   = "Tcp"
#    source_address_prefix      = "104.215.146.97/32"
#    source_port_range          = "*"
#    destination_port_range     = "22"
#    destination_address_prefix = "*"
#  }

#  tags = {
#    env = var.env
#    vm = "vm-${var.env}-${var.project}-${var.service}-bastion"
#  }
#}

# Creating public ip for NIC
resource "azurerm_public_ip" "prdapplication_public_ip" {

  name = "prdapplication-pip"
  location = var.location
  resource_group_name = var.resource_group_name
  allocation_method   = "Static"
  sku                 = "Standard"

  tags = {
    env = var.env
  }
}

locals {
  prdapplication_public_ip_id = element(concat(azurerm_public_ip.prdapplication_public_ip.*.id, list("")), 0)
}


module "vm_kaprdapp01" {
  source = "git::https://TinTranVan@bitbucket.org/tvtinpersonel/tf-az-modules.git//virtual_machine/vm_data_disk"
  #module_depends_on = [azurerm_network_security_group.sg_bastion.id]

  vm_name = "kaprdapp01"

  public_vm = true
  public_ip_id = local.prdapplication_public_ip_id

  
  env = var.env
  location = var.location
  resource_group_name = var.resource_group_name
  project = var.project
  service = var.service
  
  # Using subnet securiry group if this machine doesn't have specific rules
  network_security_group_id = var.sg_KAPrdAppSubnet


  # Using security group above if this machine using other security group, not the same with subnet security group
  #network_security_group_id = azurerm_network_security_group.sg_bastion.id # get id from above
  
  subnet_id = var.subnet_KAPrdAppSubnet

  # Adding this machine to lb backend pool 
  lb_backend_id = [module.lb_internal_kaprdapp.azurerm_lb_backend_address_pool_id[0]]

  vm_size = "Standard_B1s"

  data_disk_option = true
  data_disk_size = "10" # Enable if data_disk_option = true

  os_publisher = "OpenLogic"
  os_offer = "CentOS"
  os_sku = "7-CI"
  os_version = "latest"
  
  os_disk_type = "Standard_LRS"

  os_admin_user = var.os_admin_user
  ssh_key_data = var.ssh_key_data

  user_data = "userdata_simple.tpl"  



  
}
