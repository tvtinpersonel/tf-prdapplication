locals {
  kaprdapp_backend_id = module.lb_internal_kaprdapp.azurerm_lb_backend_address_pool_id[0]
  kaprdapp_backend_02_id = module.lb_internal_kaprdapp.azurerm_lb_backend_address_pool_id[1]
}

module "lb_internal_kaprdapp" {
    source = "git::https://TinTranVan@bitbucket.org/tvtinpersonel/tf-az-modules.git//networking/lb/internal_lb"
    
    lb_name = "lb_internal_kaprdapp"
    env = var.env
    location = var.location
    resource_group_name = var.resource_group_name

    frontend_subnet_id                     = var.subnet_KAPrdAppSubnet
    frontend_private_ip_address_allocation = "Dynamic"


    backend_address_pool = ["kaprdapp_backend","kaprdapp_backend_02"] # Creating 2 backne pool

    lb_port = {
        app  = ["8096", "Tcp", "8096", local.kaprdapp_backend_id] # rule for backend 01
        https = ["443", "Tcp", "443", local.kaprdapp_backend_02_id] # rules for backend 02
  }

    tags = {
        env = var.env
        source      = "terraform"
  }
}