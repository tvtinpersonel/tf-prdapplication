##### kaprdweb01 output

output "vm_kaprdweb01_ip" {
  value = module.vm_kaprdweb01.vm_data_ip
}

output "vm_kaprdweb01_id" {
  value = module.vm_kaprdweb01.vn_data_id
}

output "vm_kaprdweb01_nic_id" {
  value = module.vm_kaprdweb01.vn_data_nic_id
}

#output "sg_bastion_id" {
#  value = azurerm_network_security_group.sg_bastion.id
#}




##### kaprdweb02 output

output "vm_kaprdweb02_ip" {
  value = module.vm_kaprdweb02.vm_data_ip
}

output "vm_kaprdweb02_id" {
  value = module.vm_kaprdweb02.vn_data_id
}

output "vm_kaprdweb02_nic_id" {
  value = module.vm_kaprdweb02.vn_data_nic_id
}

#output "sg_bastion_id" {
#  value = azurerm_network_security_group.sg_bastion.id
#}

##### kaprdapp01 output

output "vm_kaprdapp01_ip" {
  value = module.vm_kaprdapp01.vm_data_ip
}

output "vm_kaprdapp01_id" {
  value = module.vm_kaprdapp01.vn_data_id
}

output "vm_kaprdapp01_nic_id" {
  value = module.vm_kaprdapp01.vn_data_nic_id
}

#output "sg_bastion_id" {
#  value = azurerm_network_security_group.sg_bastion.id
#}


##### kaprdapp02 output

output "vm_kaprdapp02_ip" {
  value = module.vm_kaprdapp02.vm_data_ip
}

output "vm_kaprdapp02_id" {
  value = module.vm_kaprdapp02.vn_data_id
}

output "vm_kaprdapp02_nic_id" {
  value = module.vm_kaprdapp02.vn_data_nic_id
}

#output "sg_bastion_id" {
#  value = azurerm_network_security_group.sg_bastion.id
#}


##### kaprdapp03 output

output "vm_kaprdapp03_ip" {
  value = module.vm_kaprdapp03.vm_data_ip
}

output "vm_kaprdapp03_id" {
  value = module.vm_kaprdapp03.vn_data_id
}

output "vm_kaprdapp03_nic_id" {
  value = module.vm_kaprdapp03.vn_data_nic_id
}

#output "sg_bastion_id" {
#  value = azurerm_network_security_group.sg_bastion.id
#}
